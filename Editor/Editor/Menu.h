#pragma once
#include <SFML/Graphics.hpp>
#include "AABBS.cpp"

class Menu
#define  MAX_NUMBER_OF_ITEMS  7
{
public:
	Menu(float width, float height,sf::RenderWindow &window );
	~Menu();
	void draw(sf::RenderWindow &window);
	void checkButton(sf::RenderWindow &window);
	sf::Text text[7];
	sf::Font font;
	inline int GetPressedItem() const { return SelectedItemsIndex; }



private:

	int SelectedItemsIndex;
	bool isButton[MAX_NUMBER_OF_ITEMS];
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
	
};

