#include "Menu.h"
#include <iostream>

Menu::~Menu()
{
}
Menu::Menu(float width, float height, sf::RenderWindow &window)
{
	// Create a graphical text to display
	if (!font.loadFromFile("Assets/arial.ttf"))
	{ 
		std::cout << "notfound Font " << std::endl;
	}
	menu[0].setFont(font);
	menu[0].setString("Play");
	menu[0].setPosition(sf::Vector2f(1500,200 ));
	isButton[0] = true;//put true to make it change to green, false to make it stay black
 

	menu[1].setFont(font);
	menu[1].setString("Quit");
	menu[1].setPosition(sf::Vector2f(1500, 350));
	isButton[1] = true;


	menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::Black);
	menu[2].setString("Grass Tile");
	menu[2].setPosition(sf::Vector2f(1300, 910));
	isButton[2] = false;

	menu[3].setFont(font);
	menu[3].setFillColor(sf::Color::Black);
	menu[3].setString("Water Tile");
	menu[3].setPosition(sf::Vector2f(1300, 810));
	isButton[3] = false;


	menu[4].setFont(font);
	menu[4].setFillColor(sf::Color::Black);
	menu[4].setString("Rock Tile");
	menu[4].setPosition(sf::Vector2f(1300, 710));
	isButton[4] = false;
	
	menu[5].setFont(font);
	menu[5].setFillColor(sf::Color::Black);
	menu[5].setString("Save Game");
	menu[5].setPosition(sf::Vector2f(1500, 300));
	isButton[5] = true;

	
	SelectedItemsIndex = 0;
}


void Menu::draw(sf::RenderWindow &window)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		window.draw(menu[i]);
	}	
}

void Menu::checkButton(sf::RenderWindow &window)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++) {
		if (isButton[i]) {
			if (Hovering(menu[i], window))
			{
				//menu[i].setOutlineThickness(4);
				menu[i].setFillColor(sf::Color::Green);

			}
			else
			{
				//menu[i].setOutlineThickness(0);
				menu[i].setFillColor(sf::Color::Black);
			}
			if (Clicked(menu[i], window))
			{


				if (i == 0)
					std::cout << "PLAY PRESSED";
				if (i == 1)


					window.close();
				//Activated when text is clicked
			}
		}
	}
}


