#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include "AABBS.cpp"
#include "DrawGrid.h"
#include "Menu.h"

using namespace sf;
using namespace std;

int main()
{	
	// Create the main window
	RenderWindow window(VideoMode(1920, 1080), "SFML works!" ,Style::Fullscreen);
	Menu menu(window.getSize().x, window.getSize().y,window);

	// Load a sprite to display
	sf::Texture texture[4];
	if (!texture[0].loadFromFile("Assets/grass32x32.png"))
		return -1;

	if (!texture[1].loadFromFile("Assets/Watertile.png"))
		return -1;

	if (!texture[2].loadFromFile("Assets/RockTile.png"))
		return -1;

	if (!texture[3].loadFromFile("Assets/RkeyButton.png"))
		return -1;

	sf::Font Font;
	Font.loadFromFile("Assets/arial.ttf");



	sf::Sprite sprite[4]; //Sprite Array
	sprite[0].setTexture(texture[0]); //SetTexture
	sprite[0].setOrigin(16, 16); //scale size
	sprite[0].setPosition(sf::Vector2f(1500, 900));  //Position

	sprite[1].setTexture(texture[1]);
	sprite[1].setOrigin(16, 16);
	sprite[1].setPosition(sf::Vector2f(1500, 800));
	

	sprite[2].setTexture(texture[2]);
	sprite[2].setOrigin(16, 16);
	sprite[2].setPosition(sf::Vector2f(1500, 700));

	sprite[3].setTexture(texture[3]);
	sprite[3].setPosition(sf::Vector2f(1200, 50));
	
	
		
	bool Active = false;
	int ActiveType = 0;
	DrawGrid Grid;


	// Start the game loop
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyReleased:
				switch (event.key.code)
				{
				case sf::Keyboard::Escape:
					window.close();
					break;
				}
				break;
				
				//Close window : exit
			case sf::Event::Closed:
				window.close();
				break;
			}
		}
		

		for (int i = 0; i<3; i++) {
			if (Hovering(sprite[i],window)) {
				sprite[i].setScale(2.5, 2.5);
				
			}	else {
				sprite[i].setScale(2, 2);	
			}
			if (Clicked(sprite[i],window)) {
				if (Active&&ActiveType == i)
					Active = true;
				else
					Active = true;
				ActiveType = i;
			}
		}
		for (int i = 0; i < 32; i++)
			for (int i2 = 0; i2 < 32; i2++)
				Grid.Grid[i][i2].setOutlineThickness(1);
		if (Active) {           //Tile[TileIndexX][TileIndexY] is being hovered on while a type is selected
			int TileIndexX = (sf::Mouse::getPosition(window).x - 50) / 32;
			int TileIndexY = (sf::Mouse::getPosition(window).y - 5) / 32;
			if (TileIndexX >= 0 && TileIndexX < 32 && TileIndexY >= 0 && TileIndexY < 32) {
				
				Grid.Grid[TileIndexX][TileIndexY].setOutlineThickness(-4);
				if(sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
					Grid.Grid[TileIndexX][TileIndexY].setTexture(&texture[ActiveType]);
					//Tile[TileIndexX][TileIndexY] has been clicked
				}
				if (sf::Keyboard::isKeyPressed(Keyboard::R)) {
					Grid.Grid[TileIndexX][TileIndexY].setTexture(NULL);
				}
			}
		}
		menu.checkButton(window);
		window.clear(Color(211, 211, 211));
		Grid.Draw(window); 
		for (int i = 0; i<3; i++)window.draw(sprite[i]);
		menu.draw(window);
		window.draw(sprite[3]);
		window.draw(menu.text[1]);
		window.display();
	}
	return 0;
}