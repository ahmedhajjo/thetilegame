#pragma once
#include <SFML/Graphics.hpp>


template<typename Object>
bool Hovering(Object & Entity, sf::RenderWindow & window)
{
	// This is the test to check if the respository is working well
	sf::Vector2i MousePos = sf::Mouse::getPosition(window); //Coordinates of mouse
	sf::FloatRect EntityPos = Entity.getGlobalBounds();      //Rectangle defining the position of Entity
	return MousePos.x >EntityPos.left && MousePos.x < EntityPos.left + EntityPos.width && MousePos.y >EntityPos.top && MousePos.y < EntityPos.top + EntityPos.height;//AABB Statement
}

template<typename Object>
bool Clicked(Object & Entity, sf::RenderWindow & window)
{
	bool Clicked = Hovering(Entity, window) && sf::Mouse::isButtonPressed(sf::Mouse::Left); //The button is being clicked if it is hovered over and the mouse is being pressed
	while (sf::Mouse::isButtonPressed(sf::Mouse::Left)&&Hovering(Entity,window))//Wait for the button to stop being clicked so that it only gets activated once per mouse press
		sf::sleep(sf::seconds(0.01));//Sleep for 1/100 of a second to save cpu
		return Clicked; //Return whether the button was clicked or not

}

