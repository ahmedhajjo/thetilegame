#include <SFML/Graphics.hpp>
#pragma once

class DrawGrid
{
public:
    DrawGrid();
	void Draw(sf::RenderWindow &window);
	sf::RectangleShape Grid[32][32];
};

